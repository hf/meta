# Working Conventions

This document describes the working conventions of the Haskell Foundation.
This simply serves to document standard practices
and ways of working that we have found beneficial.

While principally maintained by the Board, this document concerns both Board
activity and employee activity. As such it is something like an HF handbook.
Anyone (Board member, HF employee, general public) is invited to suggest
changes to these working conventions if a better way exists.

## Making decisions

We want the Board to work together to make decisions; this requires building
consensus. However, in a world of busy people, we sometimes struggle to find
the time to review each others' work and officially approve it.

We therefore adopt the following working conventions:

1. All changes to this `meta` repo must be approved by the Board. However,
   GitLab does *not* enforce this policy: we all have the credentials to merge
   MRs.

1. All changes to this repo are submitted by Merge Request (MR), not by direct
   commit.

1. Clicking the Approve button in GitLab indicates a vote to accept the change.
   (Reacting with, say, a thumbs-up emoji expresses general support for the MR
   but is not a vote to accept.)

1. Any Board member may choose to merge the MR when the MR has sufficient
   approval.

1. The MR might *not* show all the approval from board members, for various
   reasons. Potential reasons: technical difficulty (a board member does not
   have access to the Approve button for some reason and sends in an approval
   by email), approval by acclamation in a board meeting.

1. To prevent abuse, both the Secretary and Vice Secretary receive an email
   notification for every commit to the `main` branch of this repo. They are
   expected to check to make sure these commits are either approved or clearly
   uncontroversial (e.g. spelling correction).

1. If an MR requires more approval than it has been given, the instigating
   board member sends an email to the group, with the format below. Bolding text
   can be useful here for the part that is most important for individuals to
   read. Also, we suggest to highlight the beginning of each section.

    1. Concise statement of the problem being solved
    1. Concise statement of the proposed solution (or multiple options, if the author is asking for
      a decision rather than approval)
    1. Discussion (details, analysis, and motivation)
    1. Call to action (please vote, specific deadline, etc.)

## Sub-groups

The HF encompasses many people, and it can be challenging for large groups of people
to collaborate to Get Things Done. Accordingly, we often break into smaller groups,
focused on a particular goal. Broadly, these groups may be classified as either Committees
or Working Groups. We define these terms here:

* A **committee** is a functional group of the Board. Committees are described by and subject to
  the [relevant rules in a separate document describing the functioning of the Board](board.md#8-committees).
  Committees tend to be chaired by Board members and filled mostly or entirely with Board members.
  Committees are concerned with the high-level governance of the HF.

* In contrast, a **working group** is a functional group under the (perhaps
  indirect) control of HF's Executive Director. By "indirect" here, we mean
  that a working group might be commissioned by any employee of the HF, not just
  the ED. A working group is charged with furthering the HF's goals to support
  the Haskell language, ecosystem, and community. It is typically (but not
  exclusively) led by an employee of the HF, and staffed with any relevant
  members of the community.

  Working groups do not generally have concrete rules around their creation and functioning,
  as they serve at the pleasure of whichever HF employee created them. Naturally,
  whoever creates them can set down rules, if they like.

## Membership

The following is a list of places where Board members are included. Whenever someone
leaves or joins the HF Board, all of these places will need to be updated:

1. Membership of [mailing lists](https://groups.google.com/): `board@hf`, `leadership-internal@hf`, and `board-internal@hf`.
1. Membership of private channels on Slack: `hf-board-internal`, `hf-leadership-internal`, `donuts-for-the-board`, and any private channels for committees.
1. Access on the [HF Drive](https://drive.google.com/drive/folders/1gxc2miCWM0gwMoA7ywUH3qY--q277Ifc?usp=sharing), and any subfolders. (It is unclear when changes propagate, so be careful.)
1. The [State of the Haskell Foundation](https://docs.google.com/spreadsheets/d/1oRoa3uTCVZMWbLbM8Q_L06Os3yLzTAcrXIxFQWI-Z_I/edit?usp=sharing), including any committees and working groups. Remember to update the Prior Board Members tab for leavers.
1. The [Haskell GitLab instance](https://gitlab.haskell.org/groups/hf/-/group_members).
1. The "Board Vote" [MR approval rule setting](https://gitlab.haskell.org/hf/meta/edit) needs to be adjusted if the number of Board members changes.
1. The [website](https://github.com/haskellfoundation/haskellfoundation.github.io).
1. Any HF Board calendar items, such as the invite list for our regular meetings.
1. Any admin privileges in the Google Workspace.
1. Various Google Groups hosted at `haskell.foundation`.

## Technical credentials

**"Vice" Policy**: All credentials afforded to position X are also afforded to position Vice X. Rationale: If X needs the access, then Vice X needs the access when X is unavailable.

1. The Chair and Vice Chair are Owners of the three HF mailing lists: `board@hf`, `leadership-internal@hf`, and `board-internal@hf`. The Executive Director is additionally an Owner of the `board@hf` list. These lists are managed at [groups.google.com](https://groups.google.com).
1. The Secretary and Vice Secretary are Owners of the `nominations@hf` mailing list. This
list is where the public can submit (self-)nominations for Board seats, a process managed
by our Secretaries. Other HF people may also be included in the distribution list if helpful.
1. The Chair, Vice Chair, Secretary, Vice Secretary, and Executive Director are Owners of the `hf` group on [Haskell's GitLab instance](https://gitlab.haskell.org/hf). All other board members are Developers in this group.
1. The membership of the [GitHub `haskellfoundation` group](https://github.com/haskellfoundation/) is flexible; members of the public can be added when it is convenient to do so. The Chair, Vice Chair, and Executive Director are Owners, and there may be other Owners outside the HF.
1. The Executive Director, Chair, and Vice Chair are all Super Admins of our Google Workspace.
   The ED (with the agreement of the Chair) may also delegate Super Admin status to others.
   Accordingly, the Chair and Vice Chair must have accounts in the Google Workspace; these
   are `chair@haskell.foundation` and `vice.chair@haskell.foundation`.

## Email accounts

This section is mostly informational and lays out how we make use of `@haskell.foundation`
email addresses. There are a few policy decisions, however; these are in **bold**.

### Users vs. Groups

Our Google Workspace instance supports two different kinds of email address: a *user* address
and a *group* address.

1. A workspace Super Admin (among lesser, currently unused admin roles) can create users and
   groups. Should we find it helpful, the Google Workspace interface allows us to designate
   a new admin role that is not a Super Admin but can create and/or remove users and groups.

1. User addresses:

    1. A user address comes with an entire Google account, with all the features one expects
       with an account: separate email inbox, calendaring, contact databases, user credentials, etc.
    1. User addresses cost money: the current rate is $6/user/month. Once we get our official non-profit
       status from the US government, this charge may disappear.
    1. User addresses can be configured to forward mail to another address, just like any other
       Google account.
    1. A user address is required for an email address to appear in the "From:" field in an email
       message.
    1. User addresses can receive mail from any email address.
    1. User addresses support an auto-reply feature, where incoming email messages are greeted
       with an immediate response. (This is often used for a holiday alert, for example.)
    1. User email is private: no one else at the organization can access a user's email.
       (This can potentially change, if we upgrade our Google Workspace to allow
       [investigations](https://apps.google.com/supportwidget/articlehome?hl=en&article_url=https%3A%2F%2Fsupport.google.com%2Fa%2Fanswer%2F9300435%3Fhl%3Den&product_context=9300435&product_name=UnuFlow&trigger_context=a).)

1. Group addresses:

    1. A group address is just an email address; it is not associated with an account.
    1. A group has a list of members. Each member generally receives a copy of each email sent to
       the group address. Members can configure their email delivery, and can choose to
       receive messages in digest form or not at all.
    1. Groups optionally have an archive feature. This archive supports various custom settings,
       including the ability for it to be fully public.
    1. It is possible to moderate a group address, where messages from outside the group (or
       outside the `@haskell.foundation` email address space) are held for moderation.
    1. It is not possible to send email from a group address.
    1. It is possible to add one group to another, creating a hierarchical structure. This is
       useful to declare, for example, that all messages about invoices should be delivered to
       the Treasurers; by making the Treasurers a group, we can update the list of Treasurers
       in one place, instead of updating them in every group where they appear.
    1. Groups addresses support an auto-reply feature, where incoming email messages are greeted
       with an immediate response.
    1. Group email content can be viewed by certain users with admin privileges.

1. Each user address comes with a reputational risk to the Haskell Foundation: because these
   addresses can send email, users with a `@haskell.foundation` email address can be seen to
   represent the HF. Misuse of this email address thus poses a potential risk.

1. Each user address also comes with a monetary and organizational cost. The monetary cost
   is noted above, but the organizational cost also comes in the form of credentialing and
   access management.

1. Accordingly, we prefer *group* addresses over *user* addresses. More specifically, a *user*
   address should be created only when one of the following holds:

    1. The email address will need to send email.
    1. The owner of the address needs special access to Google Workspace resources.

### Longevity

1. Individual affiliates (employees, Board members, committed volunteers) of the HF may request an
   `@haskell.foundation` email address. **The ED is responsible for granting these requests and managing
   our email namespace.**

1. All email addresses come with an additional risk of being ignored.
   If someone reaches out to the HF with email to a `@haskell.foundation` address, they reasonably
   expect their email to be read and responded to in a timely fashion. Each address we support
   thus increases our risk for lost communication.

1. Accordingly, it is good practice to review our email addresses periodically and retire
   users and groups that are no longer active, once we are sure that the address will no longer
   be used.

1. When an individual affiliated with the HF ends
   their affiliation, they must decide what to do with their email address. As a courtesy,
   **this address can remain active beyond the end of their affiliation, up to a maximum of 6 months.
   The lifetime of an address can be extended by decision by the ED or Chair/Vice Chair of the Board.**

1. When retiring an email address, if we expect that lingering references to the address exist,
   it can be converted into a group containing the special member `deprecated@haskell.foundation`
   (described more fully below). The group is then set up with an auto-reply that directs the sender
   where they should address their email in the future.

1. The group `deprecated@haskell.foundation` is a catch-all to receive email sent to deleted
   email addresses. It can be monitored periodically to see if we are missing any important
   communication. In addition, it is a useful search term in the Groups control panel for filtering
   groups that are deprecated.

### Group Membership

1. The groups `chairship@haskell.foundation`, `treasury@haskell.foundation`, and `secretariat@haskell.foundation` exist. Each group contains the appropriate Officer and Vice Officer or the Board.

1. Various important functions of the HF are expressed as groups. (Examples: receiving job applications,
   offers to volunteer, offers of sponsorship.) **Each such group must have a minimum of at least two
   members.** If only one employee of the HF is a suitable member, then typically a Board member
   will join the group, possibly using the Officer groups introduced above. Each such group should
   generally have one designated member who is responsible for responding (e.g. the ED).
