---
title: "Bylaws of Haskell.org, Inc."
geometry: margin=1in
---
# 1. Name

The name of this corporation shall be Haskell.org, Inc.  In addition to that name, the business of the corporation may also be conducted as Haskell Foundation or The Haskell Foundation.

# 2. Purposes and Powers

[comment]: <> (This "purposes and powers" shouldn't be changed.  This language is very formulaic, because the U.S. federal tax authority needs to approve it in order to get us our tax exemption.)

Haskell.org, Inc. (the "Corporation") is organized and shall be operated exclusively for charitable purposes within the meaning of Section 501(c)(3) of the Internal Revenue Code of 1986, as amended (the “Code”). The Corporation is formed to promote scientific progress and education relating to the Haskell programming language and related technologies, and the Corporation shall undertake the following activities in furtherance of that purpose:

(a) conducting and encouraging scientific research into the theory and practice of programming language design and implementation in relation to the Haskell programming language;<br/>
(b) producing and maintaining software, including compilers and associated tools, that supports continued research and the application of that research in the Haskell community;<br/>
(c) producing and maintaining educational and informative materials for researchers, developers, users, and students within the Haskell community;<br/>
(d) creating, maintaining, hosting, and otherwise providing shared assets and infrastructure for the benefit of the Haskell community;<br/>
(e) coordinating educational activities, including mentorship programs;<br/>
(f) organizing events, seminars, and academic conferences regarding the Haskell language; and<br/>
(g) all lawful activities that may be useful in accomplishing the foregoing purposes.

In furtherance of the foregoing purposes, the Corporation shall have all the general powers enumerated in Section 202 of the N-PCL and such other powers as are now or hereafter permitted by law for a corporation organized for the foregoing purposes, including, without limitation, the power to (a) solicit grants and contributions for any corporate purpose, (b) maintain a fund or funds of real and/or personal property in furtherance of such purposes, and (c) organize one or more partially- or wholly- owned organizations.

The Corporation shall not, directly or indirectly, engage in or include among its purposes any of the activities mentioned in subparagraphs (a) through (v) of Section 404 of the N-PCL without first obtaining the approvals or consents required in such subsections.

The Corporation shall not provide, or engage in the practice of, any professional service as defined by Title 8 of the New York State Education Law.

Notwithstanding any other provision of this Certificate, the Corporation is organized exclusively for charitable purposes, and intends at all times to qualify and remain qualified as exempt from federal income tax under Section 501(c)(3) of the Code, and in connection therewith:

(a) the Corporation is not formed for and shall not be conducted nor operated for pecuniary profit or financial gain, and no part of its assets, income or profit shall be distributed to or inure to the benefit of any director, officer or other private individual or individuals, provided that nothing herein shall prevent the Corporation from paying reasonable compensation to any person for services rendered to or for the Corporation in furtherance of one or more of its purposes;<br/>
(b) no substantial part of the activities of the Corporation shall be devoted to the carrying on of propaganda or otherwise attempting to influence legislation, except to the extent permitted by the Code, whether pursuant to an election under Section 501(h) or otherwise, and no part of the activities of the Corporation shall be devoted to participating or intervening in, including the publication or distribution of statements regarding, any political campaign on behalf of or in opposition to any candidate for public office, and the Corporation will not engage in any other activities that would cause it to be characterized as an “action organization” as defined in Treasury Regulation § 1.501(c)(3)-1, promulgated under the Code; and<br/>
(c) the Corporation shall not engage in or include among its purposes any activities not permitted to be carried on by either a corporation exempt from federal income taxation under Section 501(c)(3) of the Code or a corporation to which contributions are deductible under Section 170(c)(2) of the Code.

# 3. Membership

## 3.1. No Membership Classes

[comment]: <> (This is important to make it clear that we will operate on a non-profit basis.  The term "member" typically refers to people who have a financial interest in the profits of a company, and we want to be very clear that we do not have anyone in that position.)

The corporation shall have no members who have any right to vote or title or interest in or to the corporation, its properties and franchises.

## 3.2. Non-Voting Affiliates
The board of directors may approve classes of non-voting affiliates with rights, privileges, and obligations established by the board.  Affiliates may be individuals, businesses, and other organizations that seek to support the mission of the corporation.  The board, a designated committee of the board, or any duly elected officer in accordance with board policy, shall have authority to admit any individual or organization as an affiliate, to recognize representatives of affiliates, and to make determinations as to affiliates’ rights, privileges, and obligations.   At the discretion of the board of directors, affiliates may be given endorsement, recognition and media coverage at fundraising activities, clinics, other events or at the corporation website. Affiliates have no voting rights, and are not members of the corporation.

## 3.3. Dues
Any dues for affiliates shall be determined by the board of directors.

# 4. Board of Directors

## 4.1. Number of Directors
Haskell.org, Inc. shall have a board of directors consisting of at least 3 directors.

### 4.1.1. Appointment of Directors
The board may appoint additional directors, up to the maximum number of directors in office at any point in the previous 12 months, at any time with the approval of a majority of the directors then in office, regardless of their presence, absence, or abstention at any vote taken for such approval.  In order to increase the size of the board of directors beyond its highest in the previous 12 months, a 2/3 majority is required.  If for any reason the number of directors in office falls below 3, the directors remaining in office shall without undue delay appoint additional directors until at least 3 are in office.

## 4.2. Powers
All corporate powers shall be exercised by or under the authority of the board and the affairs of the Haskell.org, Inc. shall be managed under the direction of the board, except as otherwise provided by law.

## 4.3. Terms
Each director shall serve for a term beginning at their appointment or re-appointment and ending after three years or ending at such other time as may be specified by the board pursuant to the [Alteration of Terms](#431-alteration-of-terms) section, below, or until that director resigns.

### 4.3.1. Alteration of Terms
The board of directors may alter or end the term of a director by a 2/3 majority vote, provided that the director whose term is altered or ended is given written notice pursuant to the [Notice for Meetings](#45-notice-for-meetings) section, below, that such a vote will be held and is given the opportunity to be heard at that meeting.  Such notice and hearing shall not be required for any director who has been absent and unexcused from 25% or more meetings of the board of directors in the twelve month period prior to the vote on removal.  Excuses for absence of directors other than the board chair may be granted by the board chair; excuses for absence of the board chair may be granted by the board vice chair.

## 4.4. Qualifications and Election of Directors
In order to be eligible to serve as a director on the board of directors, the individual must be of the age of majority in both the state of New York and their place of residence, and their directorship must not be legally prohibited by the laws of the state of New York or those of their place of residence.

## 4.5. Notice for Meetings

### 4.5.1. Timing and Delivery

Board meetings shall be held upon ten (10) days notice by first-class mail, four (4) days notice by electronic mail, or forty-eight (48) hours notice delivered personally or by telephone.  If sent by mail or electronic mail, the notice shall be deemed to be delivered upon its deposit in the mail or transmission system.  Notice by telephone is effective only if the director or their designated agent answers the phone personally; voicemail and similar services shall not be effective.  Notice of meetings shall specify the day and hour of the meeting as well as the place of the meeting, if an in-person meeting, or clear instructions for connecting to the meeting, if a virtual meeting.  The purpose of the meeting need not be specified.  All directors must keep the Secretary informed of their current postal address, telephone number, and email address at all times, so that they can be notified according to the above procedures.

### 4.5.2. Waiver
Any director may waive notice of any meeting, in accordance with New York law.  A director’s presence at a meeting shall constitute waiver of notice for that meeting, unless that director explicitly notes their refusal to waive notice during the meeting, prior to participating in any votes during that meeting.

## 4.6. Manner of Acting

### 4.6.1. Quorum
A quorum of directors shall consist of a simple majority of the directors currently in office, unless otherwise specified in these Bylaws.

### 4.6.2. Majority Vote
Except as otherwise required by law or by the Certificate of Incorporation, the act of the majority of the directors present at a meeting at which a quorum is present shall be the act of the board.

### 4.6.3. Hung Board Decisions
On the occasion that directors of the board are unable to make a decision based on a tied number of votes, the board chair shall have the tie-breaker vote.

### 4.6.4. Participation
Except as required otherwise by law, the Certificate of Incorporation, or these Bylaws, directors may participate in a regular or special meeting through the use of any means of communication by which all directors participating may simultaneously hear each other during the meeting, including in person, internet video meeting or by telephonic conference call.

## 4.7. Compensation for Board Service
Directors shall receive no compensation for carrying out their duties as directors.  The board may adopt policies providing for reasonable reimbursement of directors for expenses incurred in conjunction with carrying out board responsibilities, such as travel expenses to attend board meetings.

## 4.8. Compensation for Professional Services by Directors
Directors are not restricted from being remunerated for professional services provided to the corporation.  Such remuneration shall be reasonable and fair to the corporation and must be reviewed and approved in accordance with the board [Conflict of Interest policy](../policies/conflict-of-interest.md) and state law.

## 4.9. Asynchronous Action By The Board of Directors
Any action required or permitted to be taken by the board of directors at a meeting may be taken without a meeting if a proposed decision is sent in writing (including email) to the Directors and that decision is approved by the Directors by a vote according to the normal procedures as specified in these Bylaws.  For the purposes of an asynchronous vote, the number of votes in favor required shall be the same as if the vote were carried out at a meeting with all Directors present.  The Secretary shall notify the board any time such a decision is made.

# 5. Committees

## 5.1. Establishment and Powers
The board of directors may, by the resolution adopted by a majority of the directors then in office, designate one or more committees, each consisting of two or more directors, to serve at the pleasure of the board.  Any committee, to the extent provided in the resolution of the board, shall have all the authority of the board, except that no committee, regardless of board resolution, may:

(a) alter its own authority or powers;

(b) take any action requiring the approval of more than a majority of the board or for which special notice requirements apply (such as dismissing a Director);

(c) fill vacancies on the board of directors;

(d) amend or repeal Bylaws or adopt new Bylaws;

(e) amend or repeal any resolution of the board of directors which does not expressly state that it can be so amended or repealed;

(f) appoint any other committees of the board of directors or the members of these committees;

(g) expend corporate funds to support a nominee for director; or

(h) approve any transaction
    (1) to which the corporation is a party and in which one or more directors have a material financial interest; or
    (2) between the corporation and one or more of its directors or between the corporation or any person in which one or more of its directors have a material financial interest.

## 5.2. Meetings and Actions
Meetings and action of the committees shall be governed by, held, and taken in accordance with their respective charters, as approved by the Board.

# 6. Officers

## 6.1. Board Officers
The board officers of the corporation shall be chosen by, and serve at the pleasure of, the board of directors.  Each officer shall have the authority and shall perform the duties set forth in these Bylaws or by resolution of the board or by direction of an officer authorized by the board to prescribe the duties and authority of other officers. The board may also appoint vice-chairs and such other officers as it deems expedient for the proper conduct of the business of the corporation, each of whom shall have such authority and shall perform such duties as the board of directors may determine.  One person may hold two or more board offices, but no board officer may act in more than one capacity where action of two or more officers is required.

## 6.2. Term of Office
Each officer shall serve a term of office prescribed by the board.

## 6.3. Removal and Resignation
The board of directors may remove an officer at any time, with or without cause and with or without providing any stated reason.  Any officer may resign at any time by giving written notice to the Secretary without prejudice to the rights, if any, of the corporation under any contract to which the officer is a party.  Any resignation shall take effect at the date of the receipt of the notice, unless otherwise specified in the notice.  The acceptance of the resignation shall not be necessary to make it effective.

## 6.4. Board Chair
The board chair shall lead the board of directors in performing its duties and responsibilities, including, if present, presiding at all meetings of the board of directors, and shall perform all other duties incident to the office or properly required by the board of directors.  The board chair shall be the primary point of contact for reporting ethics violations as described in the [Codes of Ethics and Whistleblower Policy](#10-codes-of-ethics-and-whistleblower-policy) section, below.

## 6.5. Vice Chair
In the absence or disability of the board chair, the vice-chair shall perform the duties of the board chair. When so acting, the vice-chair shall have all the powers of and be subject to all the restrictions upon the board chair.  The vice-chair shall have such other powers and perform such other duties prescribed for them by the board of directors or the board chair.  The vice chair shall be the secondary point of contact for reporting ethics violations as described in the [Codes of Ethics and Whistleblower Policy](#10-codes-of-ethics-and-whistleblower-policy) section, below.

## 6.6. Secretary
The secretary shall keep or cause to be kept a book of minutes of all meetings and actions of the board of directors.  The minutes of each meeting shall state the date and place or communication channel in which it was held and such other information as shall be necessary to determine the actions taken and whether the meeting was held in accordance with the law and these Bylaws.  The secretary shall cause notice to be given of all meetings of directors and committees as required by the Bylaws.  The secretary shall obtain Conflict of Interest Disclosure Statements from all people covered by the Conflict of Interest Policy periodically as that Policy prescribes.  The secretary shall have such other powers and perform such other duties as may be prescribed by the board of directors or the board chair.  The secretary may appoint, with approval of the board, a director to assist in performance of all or part of the duties of the secretary.

## 6.7. Treasurer
The treasurer shall be the lead director for oversight of the financial condition and affairs of the corporation.  The treasurer shall oversee and keep the board informed of the financial condition of the corporation and of audit or financial review results.  In conjunction with other directors or officers, the treasurer shall oversee budget preparation and shall ensure that appropriate financial reports, including an account of major transactions and the financial condition of the corporation, are made available to the board of directors on a timely basis or as may be required by the board of directors.  The treasurer shall perform all duties properly required by the board of directors or the board chair.  The treasurer may appoint, with approval of the board, a qualified fiscal agent or member of the staff to assist in performance of all or part of the duties of the treasurer.

## 6.8. Non-Director Officers
The board of directors may designate additional officer positions of the corporation and may appoint and assign duties to other non-director officers of the corporation.

# 7. Contracts, Checks, Loans, Indemnification, and Related Matters

## 7.1. Contracts and other Writings
Except as otherwise provided by resolution of the board or board policy, all contracts, deeds, leases, mortgages, grants, and other agreements of the corporation shall be executed on its behalf by the treasurer or other persons to whom the corporation has delegated authority to execute such documents in accordance with policies approved by the board.

## 7.2. Checks, Drafts
All checks, drafts, or other orders for payment of money, notes, or other evidence of indebtedness issued in the name of the corporation, shall be signed by such officer or officers, agent or agents, of the corporation and in such manner as shall from time to time be determined by resolution of the board.

## 7.3. Deposits
All funds of the corporation not otherwise employed shall be deposited from time to time to the credit of the corporation in such banks, trust companies, or other depository as the board or a designated committee of the board may select.

## 7.4. Loans
No loans shall be contracted on behalf of the corporation and no evidence of indebtedness shall be issued in its name unless authorized by resolution of the board. Such authority may be general or confined to specific instances.

## 7.5. Indemnification

### 7.5.1. Mandatory Indemnification
The corporation shall indemnify a director or former director, who was wholly successful, on the merits or otherwise, in the defense of any proceeding to which that director was a party by virtue of being or having been a director of the corporation against reasonable expenses incurred by them in connection with the proceedings.

### 7.5.2. Permissible Indemnification
The corporation shall indemnify a director or former director made a party to a proceeding by virtue of being or having been a director of the corporation, against liability incurred in the proceeding, if the determination to indemnify them has been made in the manner prescribed by the law and payment has been authorized in the manner prescribed by law.

### 7.5.3. Advance for Expenses
Expenses incurred in defending a civil or criminal action, suit or proceeding may be paid by the corporation in advance of the final disposition of such action, suit or proceeding, as authorized by the board of directors in the specific case, upon receipt of (I) a written affirmation from the director, officer, employee or agent expressing a good faith belief that they are entitled to indemnification as authorized in this [Contracts, Checks, Loans, Indemnification, and Related Matters](#7-contracts-checks-loans-indemnification-and-related-matters) section, and (II) an undertaking by or on behalf of the director, officer, employee or agent to repay such amount, unless it shall ultimately be determined that they are entitled to be indemnified by the corporation in these Bylaws.

### 7.5.4. Indemnification of Officers, Agents and Employees
An officer of the corporation who is not a director is entitled to mandatory indemnification under this [Contracts, Checks, Loans, Indemnification, and Related Matters](#7-contracts-checks-loans-indemnification-and-related-matters) section to the same extent as a director.  The corporation may also indemnify and advance expenses to an employee or agent of the corporation who is not a director, consistent with New York Law and public policy, provided that such indemnification, and the scope of such indemnification, is set forth by the general or specific action of the board or by contract.

# 8. Miscellaneous

## 8.1. Headings, Markup, and Comments
The headings contained in these Bylaws are for reference purposes only and shall not affect in any way the meaning or interpretation of these Bylaws.

These Bylaws are written using the Markdown textual document format, which is intended to be rendered into a final document by automated tools.  Only the content of the final rendered document shall have legal effect; comments, formatting, the introductory YAML block, and other syntactic structures in the original source code for these Bylaws shall not affect in any way the meaning or interpretation of these Bylaws, even if such comments include information about the authors' intent or the drafting process.

## 8.2. Books and Records
The corporation shall keep correct and complete books and records of account and shall keep minutes of the proceedings of all meetings of its board of directors, a record of all actions taken by board of directors without a meeting, and a record of all actions taken by committees of the board.  In addition, the corporation shall keep a copy of the corporation’s Certificate of Incorporation and Bylaws as amended to date.

## 8.3. Fiscal Year
The fiscal year of the corporation shall be from January 1 to December 31 of each year.

## 8.4. Conflict of Interest
The board shall adopt and periodically review a conflict of interest policy to protect the corporation’s interest when it is contemplating any transaction or arrangement which may benefit any director, officer, employee, affiliate, or member of a committee with board-delegated powers.

## 8.5. Nondiscrimination Policy
The officers, directors, committee members, employees, and persons served by this corporation shall be selected entirely on a nondiscriminatory basis with respect to age, sex, race, religion, national origin, and sexual orientation.  It is the policy of Haskell.org, Inc. not to discriminate on the basis of race, creed, ancestry, marital status, gender, sexual orientation, age, physical disability, veteran’s status, political service or affiliation, color, religion, or national origin.

# 9. Transparency and Accountability

## 9.1. Purpose
By making full and accurate information about its mission, activities, finances, and governance publicly available, Haskell.org, Inc. practices and encourages transparency and accountability to the general public. This policy will:

(a) indicate which documents and materials produced by the corporation are presumptively open to staff and/or the public

(b) indicate which documents and materials produced by the corporation are presumptively closed to staff and/or the public

(c) specify the procedures whereby the open/closed status of documents and materials can be altered.

The details of this policy are as follow:

## 9.2. Financial and IRS documents (The form 1023 and the form 990)
Haskell.org, Inc. shall provide its Internal Revenue forms 990, 990-T, 1023 and 5227, bylaws, conflict of interest policy, and financial statements to the general public for inspection free of charge.

## 9.3. Means and Conditions of Disclosure
Haskell.org, Inc. shall make “Widely Available” the aforementioned documents on or linked from its website, haskell.foundation, to be viewed and inspected by the general public.

(a) The documents shall be posted in a format that allows an individual using the Internet to access, download, view and print them in a manner that faithfully reproduces the image of the original document filed with the IRS (except information exempt from public disclosure requirements, such as contributor lists).

(b) The website shall clearly inform readers that the document is available and provide instructions for downloading it.

(c) Haskell.org, Inc. shall not charge a fee for downloading the information. Documents shall not be posted in a format that would require special computer hardware or software (other than software readily available to the public free of charge).

(d) Haskell.org, Inc. shall inform anyone requesting the information where this information can be found, including the web address. This information must be provided immediately for in-person requests and within 7 days for mailed requests.

## 9.4. IRS Annual Information Returns (Form 990)
Haskell.org, Inc. shall submit the Form 990 to its board of directors prior to the filing of the Form 990. While neither the approval of the Form 990 or a review of the 990 is required under Federal law, the corporation’s Form 990 shall be submitted to each member of the board of director’s via (hard copy or email) at least 10 days before the Form 990 is filed with the IRS.

# 10. Codes of Ethics and Whistleblower Policy

[comment]: <> (Putting this in our bylaws helps us with 501(c)(3) status.)

## 10.1. Purpose
Haskell.org, Inc. requires and encourages directors, officers and employees to observe and practice high standards of business and personal ethics in the conduct of their duties and responsibilities. The employees and representatives of the corporation must practice honesty and integrity in fulfilling their responsibilities and comply with all applicable laws and regulations. It is the intent of Haskell.org, Inc. to adhere to all laws and regulations that apply to the corporation and the underlying purpose of this policy is to support the corporation’s goal of legal compliance. The support of all corporate staff is necessary to achieving compliance with various laws and regulations.

## 10.2. Reporting Violations
If any director, officer, staff or employee reasonably believes that some policy, practice, or activity of Haskell.org, Inc. is in violation of law, a written complaint must be filed by that person with the board chair or vice chair.

## 10.3. Acting in Good Faith
Anyone filing a complaint concerning a violation or suspected violation of the Code must be acting in good faith and have reasonable grounds for believing the information disclosed indicates a violation of the Code. Any allegations that prove not to be substantiated and which prove to have been made maliciously or knowingly to be false shall be viewed as a serious disciplinary offense.

## 10.4. Retaliation
Said person is protected from retaliation if they bring the alleged unlawful activity, policy, or practice to the attention of Haskell.org, Inc. and provides the Haskell.org, Inc. with a reasonable opportunity to investigate and correct the alleged unlawful activity. The protection described below is only available to individuals that comply with this requirement.

Haskell.org, Inc. shall not retaliate against any director, officer, staff or employee who in good faith, has made a protest or raised a complaint against some practice of Haskell.org, Inc. or of another individual or entity with whom Haskell.org, Inc. has a business relationship, on the basis of a reasonable belief that the practice is in violation of law, or a clear mandate of public policy.

Haskell.org, Inc. shall not retaliate against any director, officer, staff or employee who discloses or threatens to disclose to a supervisor or a public body, any activity, policy, or practice of Haskell.org, Inc. that the individual reasonably believes is in violation of a law, or a rule, or regulation mandated pursuant to law or is in violation of a clear mandate of public policy concerning the health, safety, welfare, or protection of the environment.

## 10.5. Confidentiality
Violations or suspected violations may be submitted on a confidential basis by the complainant or may be submitted anonymously. Reports of violations or suspected violations shall be kept confidential to the extent possible, consistent with the need to conduct an adequate investigation.

## 10.6. Handling of Reported Violations
The board chair or vice chair shall notify the sender and acknowledge receipt of the reported violation or suspected violation within fifteen business days. All reports shall be promptly investigated by the board and its appointed committee and appropriate corrective action shall be taken if warranted by the investigation.

This policy shall be made available to all directors, officers, staff or employees and they shall have the opportunity to ask questions about the policy.

# 11. Amendment

## 11.1. Bylaws
These Bylaws may be amended, altered, repealed, or restated by a vote of a 2/3 majority of the board of directors then in office at a meeting of the Board, provided, however,

(a) that no amendment shall be made to these Bylaws which would cause the corporation to cease to qualify as an exempt corporation under Section 501 (c)(3) of the Internal Revenue Code of 1986, or the corresponding section of any future Federal tax code;  and,

(b) that all amendments be consistent with the Certificate of Incorporation.

## 11.2. Certificate of Incorporation
Any amendment to the Certificate of Incorporation may be adopted by approval of two-thirds (2/3) of the board of directors.
