# Transparency

This document sets out the transparency principles and practices of the
Haskell Foundation (HF).

## Principle

The HF is the servant of the Haskell community; it seeks to _support_ and
_empower_ the efforts of that community; and it will succeed only if it enjoys
the _trust_ of the community.  To that end, the HF strives to be transparent
in the following ways.  A member of the Haskell community should find it easy:

1. to discover what the HF has done;
1. to discover what the HF is planning to do;
1. to offer suggestions and ideas to the HF for what it might do, or do better; and
1. to know who influences HF decisions, and how.

## Practices

To meet the above transparency goals, the HF has the following practices:

1. Minutes for meetings are posted publicly in accordance with
   the [communications](communications.md) document.

1. The Haskell Foundation [calendar] is public.

1. Every month, the Haskell Foundation produces an update on its activities
   for the previous month. These are posted on the website and on other fora.

1. Every month, the Haskell Foundation holds an open meeting ("Office Hours").
   These meetings are posted on the HF [calendar] and advertised online. The
   goal of these meetings is to make the HF available
   to the public to answer questions and to source ideas from the community.

1. Much communication within the Haskell Foundation Board is publicly archived; details
   are in the [communication](communication.md) document.

1. The Haskell Foundation maintains a [proposals](https://github.com/haskellfoundation/tech-proposals/)
   repository, where anyone can submit a proposal for an idea that the HF should
   pursue. This repository is the one official way to request that the HF take
   a specific action.

1. A public forum (currently, [Discourse](https://discourse.haskell.org/)) is provided for HF-related discussion.
   Details are in the [communication](communication.md) document.

1. The Haskell Foundation maintains an up-to-date [State of the Haskell Foundation](https://docs.google.com/spreadsheets/d/1oRoa3uTCVZMWbLbM8Q_L06Os3yLzTAcrXIxFQWI-Z_I), which details the people and structures
that form the HF.

1. The Haskell Foundation publishes its list of donors and their respective donation tier semiannually on its website.
   This listing respects requests for anonymity for donors.

1. The Haskell Foundation publishes its annual budget on its website.

[calendar]: https://calendar.google.com/calendar/u/0/embed?src=g5pug6rj631f31d4ief6avdodc@group.calendar.google.com
