# Conflict of Interest Policy

This document describes the conflict of interest policy for the Haskell Foundation, Inc.
("the Corporation"). This policy was adapted from [this original](https://www.councilofnonprofits.org/sites/default/files/member-resources/files/Sample-Conflict-of-Interest-Policy-small-nonprofits-NNY.docx).

## 1. Definitions

1.  **Key person** means a person, other than a director or officer, whether or not an employee of the Corporation, who:

    1. a has responsibilities, or exercises powers or influence over the Corporation as a whole similar to the responsibilities, powers, or influence of directors and officers;

    2. manages the Corporation, or a segment of the Corporation that represents a substantial portion of the activities, assets, income, or expenses of the Corporation; or

    3. alone or with others controls or determines a substantial portion of the Corporation’s capital expenditures or operating budget.

2.   **Relative** means a person’s spouse or domestic partner, ancestors, brothers and sisters (whether whole or half-  blood), children (whether natural or adopted), grandchildren, great-grandchildren, and spouses or domestic partners of brothers, sisters, children, grandchildren and great-grandchildren.

3.  A **related party** is:

    1. a director, officer or key person of the Corporation or any affiliate of the Corporation, or

    2. a relative of any individual described in (1), or

    3. an entity in which any individual described in (1) or (2) has an ownership or beneficial interest of 35% or more, or in the case of a partnership or professional Corporation, a direct or indirect ownership interest in excess of 5%.

4.  A transaction is not a related party transaction if:

    1. the transaction, or the related party’s financial interest in the transaction, is *de minimis*;

    2. the transaction would not customarily be reviewed by the board or the boards of similar organizations in the ordinary course of business and is available to others on the same or similar terms;

    3. the transaction constitutes a benefit provided to a related party solely as a member of a class of the beneficiaries that the Corporation intends to benefit as part of the accomplishment of its mission (and that benefit is available to all similarly situated members of the same class on the same terms).

## 2. Overview

### 1. Purpose

The purpose of this Conflict of Interest and Compensation Policy (the “policy”) is to protect the Corporation’s interests when it is considering taking an action or entering into a transaction that might benefit the private interests of a director, officer or key person, result in the payment of excessive compensation to a director, officer or key person; or otherwise violate state and federal laws governing conflicts of interest applicable to nonprofit, charitable organizations.

### 2. Why is a policy necessary?

As a nonprofit, charitable organization, the Corporation is accountable to both government agencies and members of the public for responsible and proper use of its resources. Directors, officers and employees have a duty to act in the Corporation’s best interests and may not use their positions for their own financial or personal benefit.

Conflicts of interest must be taken very seriously since they can damage the Corporation’s reputation and expose both the Corporation and affiliated individuals to legal liability if not handled appropriately. Even the appearance of a conflict of interest should be avoided, as it could undermine public support for the Corporation.

### 3. To whom does this policy apply?

This policy applies to all directors, officers and key persons (“you”).

## 3. Identifying Conflicts of Interest

### 1. What is a conflict of interest?

A potential conflict of interest arises when a director, officer or key person, or that person’s relative or business (a) stands to gain a financial benefit from an action the Corporation takes or a transaction into which the Corporation enters; or (b) has another interest that impairs, or  could be seen to impair, the independence or objectivity of the director, officer or key person in discharging their duties to the Corporation.

### 2. What are some examples of potential conflicts of interest?

It is impossible to list all the possible circumstances that could present conflicts of interest. Potential conflicts of interest include situations in which a director, officer or key person or that person’s relative or business: 

* has an ownership or investment interest in any third party that the Corporation deals with or is considering dealing with;
* serves on the board of, participates in the management of, or is otherwise employed by or volunteers with any third party that the Corporation deals with or is considering dealing with;
* receives or may receive compensation or other benefits in connection with a transaction into which the Corporation enters;
* receives or may receive personal gifts or loans from third parties dealing with the Corporation;
* serves on the board of directors of another nonprofit organization that is competing with the Corporation for a grant or contract;
* has a close personal or business relationship with a participant in a transaction being considered by the Corporation;
* would like to pursue a transaction being considered by the Corporation for  their personal benefit.

In situations where you are uncertain, err on the side of caution and disclose the potential conflict as set forth in Section 4 of this policy.

**A potential conflict is not necessarily a conflict of interest.** A person has a conflict of interest only if the board of directors (“board”) decides, pursuant to Section 5 of this policy, that a conflict of interest exists.

## 4. Disclosing Potential Conflicts of Interest

1. You must disclose to the best of your knowledge all potential conflicts of interest as soon as you become aware of them and always before any actions involving the potential conflict are taken.  Submit a signed, written statement disclosing all the material facts to the board of directors. A [template](conflict-of-interest-statement.md) is available.

2. You must file an annual disclosure statement in the form attached to this policy. If you are a director, you must also file this statement prior to your initial election.  Submit the form to the secretary of the board of directors.

3. The secretary of the board of directors is responsible for ensuring that every director
has a disclosure statement in the *Conflict of Interest Declarations* folder in our Google Drive account. As of May 1 of every year, these statements must all be dated within the past year.

## 5. Determining Whether a Conflict of Interest Exists

1. After there has been disclosure of a potential conflict and after gathering any relevant information from the concerned director, officer or key person, the board of directors shall determine whether there is a conflict of interest. The director, officer or key person shall not be present for deliberation or vote on the matter and must not attempt to influence improperly the determination of whether a conflict of interest exists.

2. In determining whether a conflict of interest exists, the board of directors shall consider whether the potential conflict of interest would cause a transaction entered into by the Corporation to raise questions of bias, inappropriate use of the Corporation’s assets, or any other impropriety.

3. A conflict always exists in the case of a related party transaction – a transaction, agreement or other arrangement in which a related party has a financial interest and in which the Corporation or any affiliate of the Corporation is a participant.

4. If the board of directors determines that there is a conflict of interest, it shall consider the matter according to the procedure below.

## 6. Procedures for Addressing a Conflict of Interest

1. When a matter involving a conflict of interest comes before the board, the board may seek information from the director, officer or key person with the conflict prior to beginning deliberation and reaching a decision on the matter.  However, a conflicted person shall not be present during the discussion or vote on the matter and must not attempt to influence improperly the deliberation or vote.

2. **Additional Procedures for Addressing Related Party Transactions**

   1. The Corporation may not enter into a related party transaction unless, after good faith disclosure of the material facts by the director, officer or key person, the board or a committee authorized by the board determines that the transaction is fair, reasonable and in the Corporation's best interest at the time of such determination.

   2. If the related party has a substantial financial interest, the board or authorized committee shall:

      1. prior to entering into the transaction, consider alternative transactions to the extent available;

      2. approve the transaction by a vote of not less than a majority of the directors present at the meeting; and

      3. contemporaneously document in writing the basis for its approval, including its consideration of any alternative transactions.

## 7. Minutes and Documentation

The minutes of any board meeting at which a matter involving a conflict of interest or potential conflict of interest was discussed or voted upon shall include:

1. the name of the interested party and the nature of the interest;

2. the decision as to whether the interest presented a conflict of interest;

3. any alternatives to a proposed contract or transaction considered by the board; and

4. if the transaction was approved, the basis for the approval.

## 8. Prohibited Acts

The Corporation shall not make a loan to any director or officer.

## 9. Procedures for Determining Compensation

1. No person shall be present for or participate in board or committee discussion or vote pertaining to:

   1.   their own compensation;

   2.   the compensation of their relative;

   3.   the compensation of any person who is in a position to direct or control them in an employment relationship;

   4.   the compensation of any person who is in a position to directly affect their financial interests; or

   5.   any other compensation decision from which the person stands to benefit.

2. In the case of compensation of Key Persons, the following additional procedures apply:

	 1.  The board or a committee authorized by the board shall approve compensation before it is paid.

   2.  The board or authorized committee shall base approval of compensation on appropriate data, including compensation paid by comparable organizations (three are sufficient if the Corporation’s income is less than $1,000,000) for functionally similar positions, availability of similar services in the geographic area of the Corporation, and compensation surveys compiled by independent firms.

   3. The board or authorized committee shall contemporaneously document:

      1. the terms of compensation and date of determination;

      2. the members of the board or committee who were present and those who voted for it;

      3. the comparability data relied on and how it was obtained;

      4. if the compensation is higher or lower than the range of comparable data, the basis for the determination, and;

      5. any actions with respect to consideration of the compensation by anyone on the board or committee who had a conflict of interest with respect to the matter.