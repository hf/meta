# Haskell Foundation, Inc. Conflict of Interest Disclosure Statement

In the text below, "Corporation" refers to Haskell Foundation, Inc., as incorporated in the state of New York, USA.
 
By signing below, I affirm that:

1. I have received and read a copy of the [Conflict of Interest and Compensation Policy](conflict-of-interest.md);
2. I agree to comply with the policy;
3. I have no actual or potential conflicts as defined by the policy or if I have, I have previously disclosed them as required by the policy or am disclosing them below.

Disclose here, to the best of your knowledge:

1. any entity in which you or a relative participates (as a director, officer, employee, owner, or member) with which the Corporation has a relationship;
2. any transaction in which the Corporation is a participant as to which you might have a conflicting interest; and
3. any other situation which may pose a conflict of interest.

Name:

Position in the Haskell Foundation:

Signature:

Date: