# Transition Committee

The Transition Committee is a committee of the Haskell Foundation Board.

## Purpose

The goal of the Transition Committee is to design and execute a search process for a new Executive Director, including handling external communication and amicable parting with the existing Executive Director.

## Responsibilities

* Design and execute a process for identifying, interviewing, and hiring a new Executive Director. Any salary adjustment would be agreed to by a majority vote of the wider board.
* Manage any external communications around this transition. The Committee is granted permission by the Board to manage these communications without a Board vote, but the Committee may seek the Board's advice if desired.

## Delegated powers

None. External communication may be handled without further Board approval, but this carries no legal significance and is thus not considered a delegated power. The Transition Committee will get Board approval on any search and hiring process, and that process will include the conditions necessary to enter an employment agreement with any next Executive Director. Similarly, any severance process will be agreed on by a separate Board vote.

## Term

This Committee's work ends when both (1) and (2) below are satisfied.

1. A new Executive Director begins their work for the Haskell Foundation.
2. The Haskell Foundation has concluded its external communications about this transition.

## Membership

1. José Pedro Magalhães, chair
2. Richard Eisenberg
3. Edward Kmett

While the existing Executive Director is not a member of the Committee, it is expected that the Committee collaborates with him to the extent possible. In particular, any external communication should be done, if at all possible, with the current Director's buy-in.

## Membership Rules

Membership changes must be approved by a vote of the Board.

## Voting Procedure

The voting procedure of this committee is the same as the procedure for the Board itself.

## Reporting

The Committee will deliver its designs for both a hiring process and a notice process to the Board. Once the hiring process has begun, it will offer regular (at least biweekly) updates via email.

## Documents

The Committee stores its working documents and deliverables in a subfolder named Transition Committee in the Committees folder in the Haskell Foundation Google Drive folder. These are accessible to the Board only.

## Committee Bylaws

Any change to this charter requires a vote of the Board, with one exception:

* The Committee may change its method of document storage and repositories.

Any exercise of this exception must be accompanied by a notification to the Board in a Board meeting or by email.
