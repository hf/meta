# Executive Committee

The Executive Committee is a committee of the Haskell Foundation Board.

## Purpose

This small committee's role is to offer direct collaboration and feedback to
the Executive Director (ED) in setting Haskell Foundation priorities and conducting
its day-to-day operation. The committee also serves as a conduit for
communication between the Board and the ED; as such, Board
members (or the general public) may privately contact Executive
Committee (EC) members in order to raise a concern or discuss an issue that
should be brought to the ED's attention.

This committee also conducts formal performance reviews of the Executive
Director.

## Responsibilities

* Hold regular meetings (perhaps weekly or biweekly) with the Executive Director (and possibly
other guests) to informally
review progress and any change in goals since the previous meeting. These meetings
serve to share ideas and brainstorm between the ED and the EC as well as to provide
a conduit for communication between the Board and the ED, giving the Board a perspective
on the recent direction of the HF.
* Communicate proactively with the wider Board, to ensure that this committee broadly
represents the will of the wider Board.
* Establish and execute a plan for routine formal performance reviews of the Executive
Director, at least once annually. These reviews include a review of salary.

## Delegated powers

None


## Term

This is a standing Committee, with no fixed term.


## Membership

- Scott Conley
- Evie Ciobanu
- Ryan Trinkle

## Membership Rules

Membership in this Committee comprises 
three members selected from among the Board, one of whom is named 
to be Chair of this Committee. The committee composition is established
by a vote to accept any update to this Charter.

## Voting Procedure

The voting procedure of this Committee is the same as the procedure for the
Board itself.

It is expected that this Committee will rarely, if ever, need to vote.


## Reporting

A key function of this Committee is communication with the wider Board.
Accordingly, this Committee proactively shares its work (mainly, the
results of the regular meetings with the ED) in weekly
email updates. The Committee is also open for questions and other communication
at any time.


## Documents

The Committee stores its working documents in the *Executive Committee* folder within
the *Committees* folder in the Haskell Foundation Google Drive folder.

These documents include the procedure for performance reviews and the
outcome of those reviews.


## Committee Bylaws

Any change to this charter requires a vote of the Board, with no exceptions.
