# Publicity Committee

## Purpose

Ensure that the Haskell Foundation's website and Twitter is updated.

## Responsibilities

1. Ensure that the website and Twitter is kept up to date by the Executive Director
2. Review, refine, and merge or reject, pull request for the website.
3. Consider suggestsions for improving the website, typically made by opening an issue on the website repository, decide which to accept, and seek volunteers or other resources to implement them.

### Approach

The members of this committee will collaborate with the Executive Director to ensure that
the website and Twitter account of Haskell Foundation is regularly updated to ensure transparency
of the procedures and promotion of Haskell Foundation.

## Delegated powers

The committee has the ability to direct the Executive Director to address website issues and to perform other public-relations tasks.
The committee does not have the power to address really controversial questions (e.g., edit sponsor credits) which should be directed to the full Board.

## Term

This is a standing (permanent) committee

## Membership

1. Niki Vazou (chair)
2. Alejandro Serrano
3. Andres Löh

## Membership Rules

The chair of the committee must be member of the board, however the committee may draw its members from the Haskell community.


## Voting Procedure

The voting procedure of this committee is the same as the procedure for the
Board itself.

## Reporting

The Committee will report to the Board regularly.

## Documents

Haskell Foundation website which is publicly available.

## Committee Bylaws
