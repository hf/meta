# Donations Committee

The Donations Committee is a committee of the Haskell Foundation Board.

## Purpose

The Donations Committee provides considered judgment around whether or
not to accept specific donations, taking into account the alignment between
the Haskell Foundation and the potential sponsor as well as any reputational
risks to the Haskell Foundation.

## Responsibilities

* To approve or deny the receipt of a donation from potential sponsors. This
process is initiated by one of three means:

  * By referral from the Executive Director
  * If a corporate donation exceeds 25% of the current year's operating budget
  * If a donation has unusual conditions attached

  Due to the importance of cadence during sales, it is expected that the Donations
  Committee respond to requests within 1 business day -- even if that response is
  a consensus that the Committee needs more time to deliberate.

  *Note:* As individual donations via the website are an automated process,
  there is no immediate review if an individual donation were to exceed 25% of
  our operating budget. Additionally, an individual donation through the website
  cannot come with conditions, and so an individual donation to be reviewed would
  be only by ED referral.

* To be aware of the HF's current set of donors, bringing any up for review
as necessary. This review is to be done at least twice per year.

* If money has already been received for a donation that this Committee deems
against the interest of the Haskell Foundation, the Committee then directs the
Executive Director how to proceed, either by (partially) refunding the donor,
redistributing the funds to appropriate charitable causes, or other action.

## Delegated powers

The Donations Committee may direct the Executive Director to reject any specific
donation, including donations that have already been received.

## Term

This is a standing committee.

## Membership

* Evie Ciobanu
* Edward Kmett
* José Pedro Magalhães

## Membership Rules

The work of this Committee is light, but potentially important. The Committee
has 3 voting members; the Executive Director has a non-voting seat as well.

Members of the Committee are added only by majority vote of the entire Board.
It is expected that the Chair of this Committee actively manage the membership,
seeking to balance opinions and inviting current members to step down and other
Board members to join the Committee.

## Voting Procedure

The voting procedure of this committee is the same as the procedure for the
Board itself.

## Reporting

The Committee will report every decision it makes to the Board. Due to the
potentially sensitive nature of these decisions, these reports will be made
on the private `leadership-internal@haskell.foundation` mailing list.

## Documents

The Committee stores its working documents and deliverables in a subfolder
named *Donation Committee* in the *Committees* folder in the Haskell
Foundation Google Drive folder.

One document stored in that folder, *Case law*, is of particular importance:
as this Committee deliberates on more cases, the *Case law* document will be
amended to reflect the Committee's thought processes and ways for it to make
future decisions.

## Committee Bylaws

Any change to this charter requires a vote of the Board, with one exceptions:

* The Committee may change its method of document storage and repositories.

Any exercise of this exception must be accompanied by a notification to the
Board in a Board meeting or by email.

## Ways of Working

What follows are guidelines for how this Committee is to operate. These are
not part of the formal charter.

The key question this Committee is to decide is: Should we accept a donation of
amount X from sponsor Y (who might be an organization or an individual)? It is
hard -- or perhaps impossible -- to set down rigid rules that would apply well
in all situations. Instead of attempting to do so, this Committee is meant to
apply its considered judgment to each case individually, building up a case law
of prior decisions that can be consulted to guide future ones.

The Executive Director will oversee the process of securing all donations (except
for small individual donations, which are automated). If there is a donation that
might be questionable, they will refer the matter to the Donations Committee. It
is in the ED's interest to make such referrals if there is any doubt, as doing
so protects the ED from making a poor (solo) decision.

In order to bootstrap this process, this document includes the following guidelines
as aspects to consider about an individual donation being considered. They are numbered
only for easy reference; no ordering or prioritization is implied. Additionally, these
are just guidelines. The Donations Committee need not make a decision referencing
any of these guidelines specifically; they are here merely to spark the decision
process.

### Legality

1. Does receiving the donation comply with US law? If the donated money comes
from an entity with government sanctions against it, accepting the money may be
illegal. Similarly, if the donated money is stolen or somehow illicit, accepting
it may be illegal.

2. Even if the donation itself is legal, is it a reasonable expectation
that the donating organization is a lawful (corporate) citizen? The HF need not
do extensive background checks, but a potential sponsor that is currently under
investigation for extensive illegal activities may give us pause.

### Public Perception

3. Is the sponsor engaged in any activity that a significant portion of our community
might find unethical or amoral? As examples, different portions of our community may find
weapons manufacturing, cryptocurrency mining, or manipulative targeted advertising to be
problematic. Before accepting donations from organizations that engage in such (or other)
activities, it may be sensible to review the impact on our public perception.

4. Would association with a particular sponsor otherwise damage our brand? For example,
if we identify the banking industry as a potential growth market, but we are considering
yet another sponsorship from yet another toy manufacturer, would that damage future prospects?

### Good Governance

5. Is the magnitude of an individual donation so large that it presents a risk due
to size? For example, if we accept one donation that is three times bigger than the rest
of our budget combined, how are we influenced by that one donor's agenda? How would
the public perceive this potential influence?

6. Does an individual donor have a reputation of causing trouble? For example, do they
have concrete expectations of quid pro quo for their donation, causing frequent communication
challenges for the HF's staff?

7. Does a donor's vision for Haskell differ significantly from the HF's? For example,
if a donor wanted Haskell to be closed-source and adopt a fee-for-use model, accepting
a donation may present a conflict of interest for the HF.